#!/usr/bin/env python

# Based on OC3K `notifyOnSncfUpdate.py`.

import requests
import json
import matplotlib.pyplot as plt
import datetime
import matplotlib.dates as mdates
import itertools
from lxml import html
from tqdm import tqdm
import csv

DATADOME = 'CENSORED'
# No need to be changed if use the button to reverse *Départ* and *Arrivée* or change *Aller*.
ITINERARY = 'XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX'

# Can be set to `None` or `datetime.timedelta(hours = N)` for instance.
# *OUIGO TRAIN CLASSIQUE* can be considered as *Trajets directs* but not *Itinéraire bis*.
# *Type de train* (*TGV INOUI* and *OUIGO*) selected from web interface works as wanted.
DURATION_UPPER_BOUND = None
SHOW_DURATION = True
# Example: datetime.datetime(2024, 11, 4)
# Can be set to `None` to only request one page.
# Especially used for development.
DEPARTURE_UPPER_BOUND = datetime.datetime(2024, 11, 4)
DISPLAY_PRICE_UPPER_BOUND = False
ONLY_CONSIDER_FIRST_CLASS_IF_THERE_IS_NO_SECOND_ONE = True
HIDE_FULL_TRAIN = False

# Source: https://www.sncf-connect.com/app/catalogue/description/carte-avantage-jeune
# *Prix valables jusqu’au 31/08/2024.* ...
CARTE_AVANTAGE_JEUNE = [
    (datetime.timedelta(), 49),
    (datetime.timedelta(hours = 1, minutes = 30), 69),
    (datetime.timedelta(hours = 3, minutes = 0), 89),
]
# Source: https://www.sncf-connect.com/aide/voyager-avec-un-animal
ANIMAL_PRICE = 7

COOKIES = {
    'datadome': DATADOME,
}

HEADERS = {
    'x-bff-key': 'ah1MPO-izehIHD-QZZ9y88n-kku876',
    # `''` does not work.
    'User-Agent': 'A',
}

PARAMS = {
    'outward': 'true',
}

response = requests.get(
    f'https://www.sncf-connect.com/bff/api/v1/itineraries/{ITINERARY}',
    PARAMS,
    cookies = COOKIES,
    headers = HEADERS,
)

captchaMessage = html.fromstring(response.text).xpath('//p[@id="cmsg"]')
if captchaMessage != [] and captchaMessage[0].text_content() == 'Please enable JS and disable any ad blocker':
    print('Request parameters expired!')
    exit(1)

def getProposals(output):
    return output['longDistance']['proposals']['proposals']

firstData = response.json()
proposals = getProposals(firstData['output'])

DATA = {
    'itineraryId': ITINERARY,
}

# Not necessary if want previous results.
DATA['next'] = True

def getTimeLabel(proposal, extremity):
    return proposal[extremity]['timeLabel']

def getDepartureDateLabel(proposal):
    # Maybe leveraging `travelId` is incorrect if the train does not start at the considered train station.
    # Note that `departure` does not mention the required year.
    # DuckDuckGo and Google `SNCF TGV "XXXX"` `XXXX` train id does not seem to help.
    departureDateLabel = datetime.datetime.strptime(proposal['travelId'].split('_')[0], '%Y-%m-%dT%H:%M')
    return departureDateLabel

with tqdm() as progressBar:
    while True:
        response = requests.post(
            'https://www.sncf-connect.com/bff/api/v1/itineraries/more',
            cookies = COOKIES,
            headers = HEADERS,
            json = DATA,
        )
        data = response.json()
        dataProposals = getProposals(data)
        for proposalIndex, proposal in enumerate(dataProposals):
            proposalDepartureDateLabel = getDepartureDateLabel(proposal)
            if proposalIndex == len(dataProposals) - 1:
                print(f'Loaded up to {proposalDepartureDateLabel}')
            if DEPARTURE_UPPER_BOUND is not None and proposalDepartureDateLabel > DEPARTURE_UPPER_BOUND:
                break
            proposals += [proposal]
            if DEPARTURE_UPPER_BOUND is None:
                break
        else:
            progressBar.update(1)
            continue
        break

durations = []
departures = []
# Maybe compute `arrivals` from `departures` and `durations`?
arrivals = []
# Maybe should use an Enum, at least a constant string to not repeat `'first'`.
classes = [
    'first',
    'second',
]
classesPrices = {class_: [] for class_ in classes}
request = firstData['request']
mainJourney = request['mainJourney']
fromToStr = f'from {mainJourney["origin"]["label"]} to {mainJourney["destination"]["label"]}'
with open(f'proposals_{fromToStr.replace(" ", "_").lower()}.csv', 'w') as csvFile:
    fieldNames = [
        'Departure datetime',
        # If there is no trip from a day to the other can leverage the departure datetime, notably for the year.
        'Arrival datetime',
        'Duration',
    ]
    for class_ in classes:
        fieldNames += [f'{class_.title()} class price']
    writer = csv.DictWriter(csvFile, fieldnames = fieldNames)

    writer.writeheader()
    for proposal in proposals:
        durationLabel = proposal['durationLabel']
        durationDatetime = datetime.datetime.strptime(durationLabel, '%Hh%M')
        durationTimeDelta = datetime.timedelta(hours = durationDatetime.hour, minutes = durationDatetime.minute)
        if DURATION_UPPER_BOUND is not None and durationTimeDelta > DURATION_UPPER_BOUND:
            continue
        for class_ in classes:
            proposalField = f'{class_}ComfortClassOffers'
            if proposalField in proposal:
                classComfortClassOffers = proposal[proposalField]['offers']
                # Are there actual not integers prices?
                classPrice = float(classComfortClassOffers[0]['priceLabel'].replace('\xa0€', '').replace(',', '.')) if classComfortClassOffers != [] else None
            else:
                classPrice = None
            classesPrices[class_] += [classPrice]
        if ONLY_CONSIDER_FIRST_CLASS_IF_THERE_IS_NO_SECOND_ONE and classesPrices['second'][-1] is not None:
            classesPrices['first'][-1] = None
        if HIDE_FULL_TRAIN and classesPrices['first'][-1] is None and classesPrices['second'][-1] is None:
            for class_ in classes:
                classesPrices[class_].pop()
            continue
        durations += [durationDatetime]
        departureDateLabel = getDepartureDateLabel(proposal)
        departures += [departureDateLabel]
        arrival = datetime.datetime.strptime(getTimeLabel(proposal, 'arrival'), '%H:%M').strftime('%-H:%M')
        arrivals += [arrival]
        row = {
            'Departure datetime': departureDateLabel,
            'Arrival datetime': arrival,
            'Duration': durationLabel,
        }
        for class_ in classes:
            row[f'{class_.title()} class price'] = classesPrices[class_][-1]
        writer.writerow(row)

def annotate(text, coordinates):
    plt.annotate(text, coordinates, horizontalalignment = 'center')

passengers = request['passengers']
CARTE_AVANTAGE_JEUNE_LABEL = 'Carte Avantage Jeune'
for passenger in passengers:
    if passenger['discountCards'][0]['label'] != CARTE_AVANTAGE_JEUNE_LABEL:
        print(f'Unexpected passenger without {CARTE_AVANTAGE_JEUNE_LABEL}!')
        exit(1)

plt.title(f'Price of trains {fromToStr} for {len(passengers)} carte(s) avantage jeune and {len(request["pets"])} animal(s)')
plt.xlabel('Departure time')
plt.ylabel('Price')
plots = []
allPrices = [price for price in itertools.chain(*classesPrices.values()) if price is not None]
priceMiddle = (max(allPrices) + min(allPrices)) / 2
for class_ in classes:
    plots += [plt.scatter(departures, classesPrices[class_], label = f'{class_.title()} class')]
for tripIndex, (arrival, departure) in enumerate(zip(arrivals, departures)):
    for class_ in classes:
        classPrice = classesPrices[class_][tripIndex]
        if classPrice is not None:
            annotate(f'{classPrice:g} €', (departure, classPrice))
for departure in departures:
    plt.axvline(departure, alpha = 0.1)

if DISPLAY_PRICE_UPPER_BOUND:
    PRICE_UPPER_BOUND_COLOR = 'green'

    def getTimeDeltaStr(timeDelta):
        return f'{timeDelta.seconds // 3_600}h{(timeDelta.seconds // 60) % 60:02}'

    for priceIntervalIndex in range(2):
        PRICE_UPPER_BOUND = CARTE_AVANTAGE_JEUNE[priceIntervalIndex][1] * CARTE_AVANTAGE_JEUNE_AMOUNT + ANIMAL_PRICE * ANIMAL_AMOUNT
        plt.axhline(PRICE_UPPER_BOUND - 1, color = PRICE_UPPER_BOUND_COLOR)
        plt.annotate(f'{PRICE_UPPER_BOUND} € {getTimeDeltaStr(CARTE_AVANTAGE_JEUNE[priceIntervalIndex][0])} - {getTimeDeltaStr(CARTE_AVANTAGE_JEUNE[priceIntervalIndex + 1][0])}', (departures[0], PRICE_UPPER_BOUND), color = PRICE_UPPER_BOUND_COLOR)

plt.xticks(departures)
xTickLabels = [departure.strftime(f'%a %-d %b\n%-H:%M\n-\n{arrival}') for departure, arrival in zip(departures, arrivals)]
ax = plt.subplot()
ax.set_xticklabels(xTickLabels)

if SHOW_DURATION:
    twinX = plt.twinx()
    durationFormatter = mdates.DateFormatter('%-Hh%M')
    twinX.yaxis.set_major_formatter(durationFormatter)
    twinX.set_ylabel('Duration')
    plots += [twinX.scatter(departures, durations, color = 'red', label = 'Duration')]
    for duration, departure in zip(durations, departures):
        annotate(duration.strftime('%-Hh%M'), (departure, duration))
labels = [plot.get_label() for plot in plots]
# Have legend outside figure to avoid dots under it.
plt.legend(plots, labels, bbox_to_anchor=(1, 1.16))
plt.show()